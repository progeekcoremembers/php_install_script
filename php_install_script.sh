#!/bin/bash

# PHP 7 install script #
# Author: Constantin Zaharia 
# Date: 04-01-2018

# Colors
CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"

# Check root access
if [[ "$EUID" -ne 0 ]]; then
	echo -e "${CRED}Sorry, you need to run this as root${CEND}"
	exit 1
fi

# current APP_PATH
APP_PATH=$(cd "$(dirname "$0")"; pwd)

# Clear log files
echo "" > /tmp/php-install-output.log
echo "" > /tmp/php-install-error.log

# PHP version to be cloned
PHP_VERSION="7.2.0"

# Repository path
REPO_PATH="/usr/local/src/php_${PHP_VERSION}"

# Directory to store config files
CONFIG_DIR="/etc/php/php-${PHP_VERSION}"

# Binaries directory name
BINARY_DIR="/usr/local/php-${PHP_VERSION}"

# Alternative symlink path
ALTERNATIVE_PATH="/usr/bin/php"

# OpenSSL lib dev package name 
PACKAGE_LIBSSL_DEV="libssl-dev"
#PACKAGE_LIBSSL_DEV="libssl-dev=1.0.2g-1ubuntu4.8"

PHP_VERSION_LOCAL="---"
PHP_BIN_PATH=$(which php)
if [[ -f $PHP_BIN_PATH ]]; then
	PHP_VERSION_LOCAL=$(php -v | head -1 | tail -n 1 | cut -d " " -f 2)
fi

# OpenSSL lib dev package name

clear
echo ""
echo "Welcome to the  PHP install script"
echo ""
echo -ne "PHP version that will be installed: ${CRED}${PHP_VERSION}$CEND"
echo ""
echo -ne "PHP local version is: ${PHP_VERSION_LOCAL}"
echo ""

#
# Build script
#

echo ""
read -n1 -r -p "PHP ${PHP_VERSION} is ready to be installed, press any key to continue..."
echo ""

# Setup Ubuntu with other dependencies for PHP 7.
# Add any missing ones from the configure script.
echo -ne "       Installing dependencies      [..]\r"
apt-get update 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
apt-get install -y build-essential \
  libldap2-dev \
  autoconf \
  libldap-2.4-2 \
  libtool-bin \
  libzip-dev \
  lbzip2 \
  libzip4 \
  libzip-dev \
  libxml2 \
  libxml2-dev \
  bzip2 \
  libbz2-dev \
  re2c \
  libbz2-dev \
  apache2-dev \
  libjpeg-dev \
  libxpm-dev \
  libxpm-dev \
  libgmp-dev \
  libgmp3-dev \
  libmcrypt-dev \
  libmcrypt4 \
  libfreetype6-dev \
  mcrypt \
  bison \
  libxslt-dev \
  libmysqlclient-dev \
  libpspell-dev \
  librecode-dev \
  libcurl4-openssl-dev \
  libssl-dev \
  libxft-dev \
  pkg-config \
  libbison-dev \
  "${PACKAGE_LIBSSL_DEV}" 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  
if [ $? -eq 0 ]; then
  echo -ne "       Installing dependencies        [${CGREEN}OK${CEND}]\r"
  echo -ne "\n"
else
  echo -e "        Installing dependencies      [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi
  
# PHP 7 does not recognize these without additional parameters or symlinks for Ldap
sudo ln -sf /usr/lib/x86_64-linux-gnu/libldap-2.4.so.2 /usr/lib/libldap.so
sudo ln -sf /usr/lib/x86_64-linux-gnu/liblber-2.4.so.2 /usr/lib/liblber.so
sudo ln -sf /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h

# make the CONFIG_DIR
mkdir -p ${CONFIG_DIR}/conf.d
mkdir -p ${CONFIG_DIR}/fpm
mkdir -p /var/run/php/php7.2.0

echo -ne "       Clone PHP ${PHP_VERSION} from sources   [..]\r"
if [ -d ${REPO_PATH}/php-src ]; then
  cd ${REPO_PATH}/php-src
  git reset --hard HEAD 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  git clean -fd 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  git pull origin "php-${PHP_VERSION}" 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  
  if [ $? -ne 0 ]; then
       cd ..
       sudo rm -rf php-src
       git clone https://github.com/php/php-src 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
       cd php-src
    fi
else
  # Obtain latest source
  mkdir -p ${REPO_PATH}
  cd ${REPO_PATH}
  git clone https://github.com/php/php-src 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  cd php-src
fi

# Checkout latest release
git checkout "php-${PHP_VERSION}" 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

if [ $? -eq 0 ]; then
  echo -ne "       Clone PHP ${PHP_VERSION} from sources   [${CGREEN}OK${CEND}]"
  echo -ne "\n"
else
  echo -e "       Clone PHP ${PHP_VERSION} from sources   [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi

echo -ne "       Configuring PHP                [..]\r"
# Helped fix configure issues and ignored files needing an update.
./buildconf --force 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

# Setup compile options for Kubuntu.  If failures occur, check dependencies and symlink needs above.
./configure --prefix="${BINARY_DIR}" \
    --with-config-file-path="${CONFIG_DIR}" \
    --with-config-file-scan-dir="${CONFIG_DIR}/conf.d" \
	--enable-fpm \
    --enable-mbstring \
    --enable-zip \
    --enable-bcmath \
    --enable-pcntl \
    --enable-ftp \
	--enable-filter \
    --enable-exif \
    --enable-calendar \
    --enable-sysvmsg \
    --enable-sysvsem \
    --enable-sysvshm \
	--enable-mysqlnd \
	--enable-opcache \
	--enable-gd-jis-conv \
	--enable-simplexml \
	--enable-xmlreader \
	--enable-xmlwriter \
    --enable-wddx \
    --enable-intl \
	--with-fpm-user=www-data \
    --with-fpm-group=www-data \
    --with-curl \
    --with-iconv \
    --with-gmp \
    --with-pspell \
	--with-gettext \
	--with-xmlrpc \
	--with-xsl \
    --with-gd \
    --with-jpeg-dir=/usr/lib \
    --with-png-dir=/usr \
    --with-zlib-dir=/usr \
    --with-xpm-dir=/usr \
    --with-freetype-dir=/usr/include/freetype2 \
    --with-openssl \
    --with-pdo-mysql=/usr \
    --with-gettext=/usr \
    --with-libzip \
    --with-bz2 \
    --with-recode=/usr \
    --with-mysqli=/usr/bin/mysql_config \
    --with-ldap 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
	
if [ $? -eq 0 ]; then
  echo -ne "       Configuring PHP                [${CGREEN}OK${CEND}]"
  echo -ne "\n"
else
  echo -e "       Configuring PHP                [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi

echo -ne "       Make PHP                       [..]\r"
# Cleanup for previous failures.
make clean 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

# Using as many threads as possible.
cpunum=$((`cat /proc/cpuinfo | grep processor | wc -l` + 1))
make -j ${cpunum} 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

# Install it according to the configured path.
make install 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

if [ $? -eq 0 ]; then
  echo -ne "       Make PHP                       [${CGREEN}OK${CEND}]"
  echo -ne "\n"
else
  echo -e "       Make PHP                       [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi

echo -ne "       Configuring PHP                [..]\r"
# It's own make script said to do this, but it didn't do much on my system.
libtool --finish ./libs 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

update-alternatives --install "$ALTERNATIVE_PATH" php "$BINARY_DIR/bin/php" 99 \
  --slave /usr/share/man/man1/php.1.gz php.1.gz "${BINARY_DIR}/php/man/man1/php.1" 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  
#update-alternatives --config php 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

if [ $? -eq 0 ]; then
  echo -ne "       Configuring PHP                [${CGREEN}OK${CEND}]"
  echo -ne "\n"
else
  echo -e "       Configuring PHP                [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi

#echo -ne "       Install PHP_Archive            [..]\r"
#pear_bin=$(which pear | wc -l)
#if [[ ${pear_bin} -eq 0 ]]; then
#  wget http://pear.php.net/go-pear.phar 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
#  php go-pear.phar 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
#  pear channel-update pear.php.net 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
#  pear config-set preferred_state alpha 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
#  pear install PHP_Archive 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
#fi

#if [ $? -eq 0 ]; then
#  echo -ne "       Install PHP_Archive            [${CGREEN}OK${CEND}]"
#  echo -ne "\n"
#else
#  echo -e "       Install PHP_Archive            [${CRED}FAIL${CEND}]"
#  echo ""
#  echo "Please look /tmp/php-install-error.log"
#  echo ""
#  exit 1
#fi

echo -ne "        Setup config files            [..]\r"
# check if the config path exists
if [ -d ${CONFIG_DIR} ]; then
   cp ${APP_PATH}/php.ini ${CONFIG_DIR}/php.ini 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
   cp -r ${APP_PATH}/fpm/* ${CONFIG_DIR}/fpm/ 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
fi

if [ $? -eq 0 ]; then
  echo -ne "       Setup config files             [${CGREEN}OK${CEND}]"
  echo -ne "\n"
else
  echo -e "       Setup config files             [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi

echo -ne "       Setup PHP-FPM systemctl script [..]\r"

if [[ ! -e ${BINARY_DIR}/php-fpm.service ]]; then
  cp ${APP_PATH}/php-fpm.service ${BINARY_DIR}/. 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
fi

if [[ ! -e /lib/systemd/system/php-fpm.service  ]]; then
  ln -s ${BINARY_DIR}/php-fpm.service /lib/systemd/system/php-fpm.service  2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  # Enable PHP FPM start at boot
  systemctl enable php-fpm.service 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
  systemctl daemon-reload 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log
fi

if [[ ! -d /var/pools/default ]]; then
  mkdir -p /var/pools/default
fi

if [ $? -eq 0 ]; then
  echo -ne "       Setup PHP-FPM systemctl script [${CGREEN}OK${CEND}]"
  echo -ne "\n"
else
  echo -e "        Setup PHP-FPM systemctl script [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
  exit 1
fi

# Restart Nginx
echo -ne "       Restarting PHP FPM            [..]\r"
systemctl restart php-fpm.service 2>> /tmp/php-install-error.log 1>> /tmp/php-install-output.log

if [ $? -eq 0 ]; then
  echo -ne "       Restarting PHP FPM             [${CGREEN}OK${CEND}]\r"
  echo -ne "\n"
else
  echo -e "       Restarting PHP FPM            [${CRED}FAIL${CEND}]"
  echo ""
  echo "Please look /tmp/php-install-error.log"
  echo ""
exit 1
fi
# We're done !
echo ""
echo -e "       ${CGREEN}Installation successful !${CEND}"
echo ""